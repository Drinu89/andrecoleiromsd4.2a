﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class bulletMovement : MonoBehaviour {

    public int moneyPerKill = 5;

    public GameObject shopPanel;

    private Shop shopScript;

    private Transform target;

    public float speed = 70f;

    public GameObject bulletHitParticle;


    //Calling the shop script 
    void Start()
    {
        shopScript = GameObject.Find("ShopPanel").GetComponent<Shop>();
    }

    public void bulletTarget(Transform _target)
    { 
        target = _target;
    }



	// Update is called once per frame
	void Update () {

        if (target == null)
        {
            Destroy(gameObject);
            
            return;
        }

        Vector3 direction = target.position - transform.position;
        float distanceThisFrame = speed * Time.deltaTime;

        if (direction.magnitude <= distanceThisFrame)
        {
            HitTarget();
            return;
        }

        transform.Translate(direction.normalized * distanceThisFrame, Space.World);

	}

    //When hitting the minion runs HitTarget() spawns the particle system and destroyed after 2 seconds;
    void HitTarget()
    {
        GameObject bulletParticle = (GameObject)Instantiate(bulletHitParticle, transform.position, transform.rotation);
        Destroy(bulletParticle, 2f);

        Destroy(target.gameObject);

        AudioSource popSound = GetComponent<AudioSource>();

        popSound.Play();//Poping sound effest is played.

        Destroy(gameObject, 3f);
        shopScript.moneyAmount += moneyPerKill;
    }
}
