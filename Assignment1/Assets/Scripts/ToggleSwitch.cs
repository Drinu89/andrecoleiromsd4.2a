﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ToggleSwitch : MonoBehaviour {
    public GameObject switchOn, switchOff;


    public void changingSound(){
        bool onoffSwitch = gameObject.GetComponent<Toggle>().isOn;

        //Sound On and Off while changing the icon too.
        if (onoffSwitch)
        {
            switchOn.SetActive(true);
            switchOff.SetActive(false);
            AudioListener.pause = !AudioListener.pause;
            Debug.Log("Switch is on");
        }
        if(!onoffSwitch)
        {
            switchOn.SetActive(false);
            switchOff.SetActive(true);
            AudioListener.pause = !AudioListener.pause;
            Debug.Log("Switch is off");
        }

        
    }
}
