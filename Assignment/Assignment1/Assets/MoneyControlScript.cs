﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class MoneyControlScript : MonoBehaviour {

    public TMP_Text moneyText;
    public static int moneyAmount;
    public GameObject turretLevelOne;

    void Start()
    {
        moneyAmount = PlayerPrefs.GetInt("MoneyAmount");
    }

    void Update()
    {
        moneyText.text = " $ " + moneyAmount.ToString();
    }
}
