﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class Health : MonoBehaviour {

    public int healthPoints = 100;
    public TMP_Text healthTxt;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        healthTxt.text = healthPoints.ToString();
	}
}
