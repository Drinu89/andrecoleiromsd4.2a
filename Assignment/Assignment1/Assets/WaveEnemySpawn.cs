﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class WaveEnemySpawn : MonoBehaviour {
    //Can add different prefabs that means different objects with different strength and speed.
    public Transform enemyPrefab;

    public Transform spawnLocation;

    public TMP_Text nextWaveInTxt;

    public TMP_Text waveNumber;

    public float secondsWaiting = 1f;
    public float countNextWave = 5f;
    private float countDown = 5f;

    //public Button btn;

    private int waveIndex = 0;

    void Start()
    {

    }

    /*void test()
    { 
        for (int i = 0; i < waveIndex; i++)
        {
            //Trying to tell if wave index = 1 and 1 ball is destroyed than pause the game
            if (waveIndex == i && GameObject.FindWithTag("Enemy")) ;
             {
                 Debug.Log("Destroyed");

             }
		}
    }*/
        
   

    public void startCounting()
    {
        
        if (countDown <= 0f)
        {
            Debug.Log("sTART");
            StartCoroutine(realeaseWave());
            countDown = countNextWave;
        }

        countDown -= Time.deltaTime;

        nextWaveInTxt.text = "Next Wave In: " + Mathf.Round(countDown).ToString();
        waveNumber.text = "Wave " + waveIndex;
        //Debug.Log(waveIndex);
    }


    void Update()
    {
       // GetComponent<UnityEngine.UI.Button>().onClick.AddListener(() => startCounting());
        startCounting();

      
       /*if (countDown <= 0f)
        {
            StartCoroutine(realeaseWave());
            countDown = countNextWave;
        }

        countDown -= Time.deltaTime;

        nextWaveInTxt.text = "Next Wave In: " + Mathf.Round(countDown).ToString();*/
    }

    IEnumerator realeaseWave()
    {
        
        Debug.Log("A Minion Rush is approacing!!");
        waveIndex++;
        for (int i = 0; i < waveIndex; i++)
        {
            SpawnEnemys();
            yield return new WaitForSeconds(secondsWaiting);
        }
        
    }


    void SpawnEnemys()
    {
        Instantiate(enemyPrefab, spawnLocation.position, spawnLocation.rotation);
    }



}
