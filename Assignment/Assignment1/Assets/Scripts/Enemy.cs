﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour {

    //Variables
    private Health healthScript;

    public float enemySpeed = 50f;

    private Transform target;
    public int wavepointIndex = 0;

    void Start()
    {
        target = EnemyWayPoints.wayPointsArray[0];

        healthScript = GameObject.Find("GameController").GetComponent<Health>();//Calling the healthScript
        
    }


    void Update()
    {
        Vector3 direction = target.position - transform.position;
        transform.Translate(direction.normalized * enemySpeed * Time.deltaTime, Space.World);

        if (Vector3.Distance(transform.position, target.position) <= 0.2f)
        {
            GetNextWaypoint();
        }
    }

    void GetNextWaypoint()
    {
        //When the Enemy hits the last WayPoint in the Finish Cube it will be destroyed
        //Health point is decreased by 1
        if (wavepointIndex == EnemyWayPoints.wayPointsArray.Length - 1)
        {
            Destroy(gameObject);
            healthScript.healthPoints -= 1;
            Handheld.Vibrate();//This will be commented when building the game for web. //Vibration when minions dies
            return;
            //return will avoid the game to give errors because destroy takes a little bit of time
        }

        //When adding the index for example "1" it needs to me matched with the target
        wavepointIndex++;
        target = EnemyWayPoints.wayPointsArray[wavepointIndex];
    }
}
