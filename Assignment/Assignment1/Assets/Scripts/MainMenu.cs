﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour {


    //Play Button pressed
    public void PlayGame() {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

    //Game quits.
    public void QuitGame() {
        Debug.Log("GoodBye! Hope you had fun playing :)");
        Application.Quit();
    }

}
