﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Health : MonoBehaviour {

    public static int Lives;
    public int healthPoints = 100;
    public TMP_Text healthTxt;

	// Use this for initialization
	void Start () {
        Lives = healthPoints;
	}
	
	// Update is called once per frame
	void Update () {
        healthTxt.text = healthPoints.ToString();

        //When health is 0 the gameover script is launched.
        if (healthPoints <= 0)
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
            Debug.Log("GameOver");
        }
	}
}
