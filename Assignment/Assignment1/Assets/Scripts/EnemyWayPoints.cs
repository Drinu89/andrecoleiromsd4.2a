﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyWayPoints : MonoBehaviour {

    public static Transform[] wayPointsArray;


    void Awake()
    {
        //ChildCount is the number of objects in an Empty Object like files in a folder
        //Minion Moves towards the way points
        wayPointsArray = new Transform[transform.childCount];
        for (int i = 0; i < wayPointsArray.Length; i++)
        {
            wayPointsArray[i] = transform.GetChild(i);
        }
    }

}
