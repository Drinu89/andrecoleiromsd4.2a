﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class Shop : MonoBehaviour {

    //Variables
    public int moneyAmount = 350;
    int isTurretSold;

    public TMP_Text moneyAmountTxt;
    public TMP_Text TurretPrice;

    public Button levelOneTurret;
    public Button levelTwoTurret;
    public Button levelThreeTurret;

    BuiltManager buildManager;

    public Button dropDown;
    public Button closeShop;

    public GameObject shopPanel;
    public GameObject shopIcons;

    void Start()
    {
        buildManager = BuiltManager.instance;
        //moneyAmount = PlayerPrefs.GetInt("MoneyAmount");
    }

    void Update() {
//
        moneyAmountTxt.text = "£" + moneyAmount.ToString();
        
        //isTurretSold = PlayerPrefs.GetInt("isTurretSold");

        //Turrets Prices
        if (moneyAmount >= 350)
            levelOneTurret.interactable = true;
        else
            levelOneTurret.interactable = false;

        if (moneyAmount >= 1500)
            levelTwoTurret.interactable = true;
        else
            levelTwoTurret.interactable = false;

        if (moneyAmount >= 5000)
            levelThreeTurret.interactable = true;
        else
            levelThreeTurret.interactable = false;
    }



    //Money is reduced on purchase
    public void PurchaseLevelOneTurret()
    {
        Debug.Log("Level 1 Turret Purchased !");
        buildManager.SetTurretToBuild(buildManager.levelOneTurret);

        moneyAmount -= 350;
    }

    public void PurchaseLevelTwoTurret()
    {
        Debug.Log("Level 2 Turret Purchased !");
        buildManager.SetTurretToBuild(buildManager.levelTwoTurretPrefab);
        moneyAmount -= 1500;
    }
    public void PurchaseLevelThreeTurret()
    {
        Debug.Log("Level 3 Turret Purchased !");
        buildManager.SetTurretToBuild(buildManager.levelThreeTurretPrefab);
        moneyAmount -= 5000;
    }

    //Show and hide shop
    public void dropShop()
    {
        closeShop.gameObject.SetActive(true);
        dropDown.gameObject.SetActive(false);
        shopPanel.gameObject.SetActive(true);
        shopIcons.gameObject.SetActive(true);
    }

    public void closeTheShop()
    {
        dropDown.gameObject.SetActive(true);
        closeShop.gameObject.SetActive(false);
        shopPanel.gameObject.SetActive(false);
        shopIcons.gameObject.SetActive(false);
    }
}
