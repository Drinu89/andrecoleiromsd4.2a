﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;


public class VolumeScript : MonoBehaviour {

    TMP_Text volumeText;
    
    

	// Use this for initialization
	void Start () {

        volumeText = GetComponent<TMP_Text>();
	}
	
	// Update is called once per frame
	void Update () {

	}

    //The text on the slider when moved
    public void volumeSliderValueChanged(float sliderValue)
    {
        volumeText.text = "VOLUME " + Mathf.RoundToInt(sliderValue * 100) + "%";
    }

    //Mute Button
    public void muteBtn() {
        Debug.Log("Sound Muted...");
        AudioListener.pause = !AudioListener.pause;
    }
}
