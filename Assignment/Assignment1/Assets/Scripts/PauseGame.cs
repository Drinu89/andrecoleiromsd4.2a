﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PauseGame : MonoBehaviour {

    public bool paused;

    public bool fastForward;

    public Button startPause;
    public Button pauseStart;

    public Button normal;
    public Button fastForwardButton;




    void Start()
    {
        paused = false;
    }

    public void resume()
    {
        pauseStart.gameObject.SetActive(true);
        startPause.gameObject.SetActive(false);
        Pause();
    }

    public void runPause()
    {
        startPause.gameObject.SetActive(true);
        pauseStart.gameObject.SetActive(false);
        Pause();

    }

    public void normalSpeed()
    {
        fastForwardButton.gameObject.SetActive(true);
        normal.gameObject.SetActive(false);
        FastForward();
    }

    public void fastSpeed()
    {
        normal.gameObject.SetActive(true);
        fastForwardButton.gameObject.SetActive(false);
        FastForward();
    }

    //TimeScale = 0 Pause, TimeScale = 1 Normal Speed, TimeScale = 5 FastForward.
    public void Pause() 
    {
        paused = !paused;

        if (paused) 
        {
            Time.timeScale = 0;
        }else if(!paused)
        {
            Time.timeScale = 1;
        }
    }

    public void FastForward()
    {
        fastForward = !fastForward;

        if (fastForward)
        {
            Time.timeScale = 5;
        }
        else if (!paused)
        {
            Time.timeScale = 1;
        }
    }
}
