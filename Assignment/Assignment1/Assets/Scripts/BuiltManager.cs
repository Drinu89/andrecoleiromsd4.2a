﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuiltManager : MonoBehaviour {

    public static BuiltManager instance;

    void Awake()
    {
        if (instance != null)
        {
            Debug.LogError("There are more than one BuildManager in the scene");
            return;
        }
        instance = this;
    }

    public GameObject levelOneTurret;
    public GameObject levelTwoTurretPrefab;
    public GameObject levelThreeTurretPrefab;

	private GameObject turretToBuild;

    public GameObject GetTurrentToBuild()
    {
        return turretToBuild;
    }

    public void SetTurretToBuild(GameObject turret)
    {
        turretToBuild = turret;
    }



	// Update is called once per frame
	void Update () {
		
	}
}
