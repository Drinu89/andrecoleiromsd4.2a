﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileScript : MonoBehaviour {

    public GridPosition TilePosition { get; private set; }



	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void Setup(GridPosition tilePos, Vector3 gamePos)
    { 
        this.TilePosition = TilePosition;
        transform.position = gamePos;
    }
}
