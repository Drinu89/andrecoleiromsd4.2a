﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameOverScript : MonoBehaviour {
    //Variables
    public TMP_Text waveTxt;

    public GameObject otherObject;

    public int maxWaves;

    private WaveEnemySpawn waveEnemyScript;



    //Deletes the PlayerPrefs Key so that score is reset.
    public void ResetHighScore()
    {
        PlayerPrefs.DeleteKey("WaveNumber");
    }

    public void Menu()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex - 3);

    }
}
