﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurretTilePlacing : MonoBehaviour {
    //Variables
    public Material hoverColor; // This variable changes the tile material
    public Vector3 turretPosition;

    private GameObject turret;

    private Renderer renderer;
    private Material matColor;

    BuiltManager buildManager;

    private GameObject shopPanel;

    private Shop shopScript;

    void Start()
    {
        renderer = GetComponent<Renderer>();
        matColor = renderer.material;

        buildManager = BuiltManager.instance;

        shopScript = GameObject.Find("ShopPanel").GetComponent<Shop>();
    }

    //OnHover
    void OnMouseEnter()
    {
        if (buildManager.GetTurrentToBuild() == null)
            return;

        renderer.material = hoverColor;
    }

    
    void OnMouseExit()
    {
        renderer.material = matColor;
    }

    //Onclick
    void OnMouseDown()
    {
        if (buildManager.GetTurrentToBuild() == null)
            return;

        //If a turret is already placed on the tile
        if (turret != null)
        {
            Debug.Log("A turret is already placed on the tile !");
            Destroy(turret);
            shopScript.moneyAmount += 300;
            return;
        }

        GameObject turretToBuild = buildManager.GetTurrentToBuild();
        turret = (GameObject)Instantiate(turretToBuild, transform.position + turretPosition, transform.rotation);
    }
}
