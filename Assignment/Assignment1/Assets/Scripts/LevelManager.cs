﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class LevelManager : MonoBehaviour {

    [SerializeField]
    private GameObject[] tilePrefab;

    public float TileWidth
    {
        get { return tilePrefab[0].GetComponent<SpriteRenderer>().sprite.bounds.size.x; }
    }

	// Use this for initialization
	void Start () 
    {
        CreateLevel();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void CreateLevel()
    {

        string[] mapData = ReadLevelText();

        int mapX = mapData[0].ToCharArray().Length;
        int mapY = mapData.Length;

        Vector3 worldStart = Camera.main.ScreenToWorldPoint(new Vector3(0, Screen.height));

        for (int y = 0; y < mapY; y++)//The y Positions of the Tiles
        {
            char[] newTiles = mapData[y].ToCharArray();

            for (int x = 0; x < mapX; x++)//The x Positions of the Tiles
            {
                //When placing the tiles in the Game
                PlaceTile(newTiles[x].ToString(),x, y, worldStart);
            }
        }

    }

    private Vector3 PlaceTile(string tileType, int x, int y, Vector3 worldStart)
    {
        int tileIndex = int.Parse(tileType);

        TileScript newTile = Instantiate(tilePrefab[tileIndex]).GetComponent<TileScript>();

        //newTile.Setup(new GridPosition(x, y), new Vector3(worldStart.x + (TileWidth * x), worldStart.y - (TileWidth * y), 0));

        return newTile.transform.position;
    }

    private string[] ReadLevelText()
    {
        TextAsset bindData = Resources.Load("Level") as TextAsset;

        string data = bindData.text.Replace(Environment.NewLine, string.Empty);

        return data.Split('-');
    }
}
