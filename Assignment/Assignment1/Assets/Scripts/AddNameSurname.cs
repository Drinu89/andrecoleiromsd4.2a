﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class AddNameSurname : MonoBehaviour {

    //Variables
    public TMP_InputField nameF;
    public TMP_InputField surnameF;
    public TMP_Text nameTxt;
    public TMP_Text surnameTxt;
    public TMP_Text messageBox;

	// Use this for initialization
	void Start () {
        /*nameTxt = GetComponent<TMP_Text>();
        surnameTxt = GetComponent<TMP_Text>();*/
	}
	
	// Update is called once per frame
	void Update () {
        
	}

    /*public void addData() {

        nameTxt.text = "Name: " + nameF.text;
        surnameTxt.text = "Surname: " + surnameF.text;

        /*string name = GameObject.Find("NameField").GetComponent<TMP_InputField>().text;
        string surname = GameObject.Find("SurnameField").GetComponent<TMP_InputField>().text;

        nameTxt.text = "Name: " + name;
        surnameTxt.text = "Surname: " + surname;
    }*/


    //Checking if username is Andre and surname is Coleiro
    public void checkLogin()
    {
        if (nameF.text == "Andre" && surnameF.text == "Coleiro")
        {
            messageBox.text = "Logged In !";
            submit();
        }
        else
        {
            messageBox.text = "Invalid Login Please Try Again";
        }
    }

    //Loads the main menu scene.
    public void submit() {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }
}
