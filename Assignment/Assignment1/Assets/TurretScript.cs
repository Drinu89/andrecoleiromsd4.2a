﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurretScript : MonoBehaviour {

    private Transform target;

   // [Header("Attributes")]

    public float fireRate = 1f;
    private float fireCountDown = 0f;
    public float rangeTurret = 15f;

   // [Header("Unity Setup Fields")]

    public string enemyTag = "Enemy";

    public Transform rotationPivot;

    //Lerp Rotation Speed
    public float rotationSpeed = 10f;

    public GameObject bulletPrefab;
    public Transform firePoint;

	// Use this for initialization
	void Start () {
        InvokeRepeating("UpdateTarget", 0f, 0.5f);
	}

    void UpdateTarget()
    {
        GameObject[] enemies = GameObject.FindGameObjectsWithTag(enemyTag);
        float shortestDistance = Mathf.Infinity;
        GameObject nearestEnemy = null;

        foreach (GameObject enemy in enemies)
        {
            float distanceToEnemy = Vector3.Distance(transform.position, enemy.transform.position);
            if (distanceToEnemy < shortestDistance) 
            {
                shortestDistance = distanceToEnemy;
                nearestEnemy = enemy;
            }
        }

        if (nearestEnemy != null && shortestDistance <= rangeTurret)
        {
            target = nearestEnemy.transform;
        }
        else {
            target = null;
        }
    }
	
	// Update is called once per frame
	void Update () {
        if (target == null)
            return;

        //Turret Rotating and lock with the Enemy
        Vector3 dir = target.position - transform.position;
        Quaternion lockToRotation = Quaternion.LookRotation(dir);
        Vector3 rotation = Quaternion.Lerp(rotationPivot.rotation, lockToRotation, Time.deltaTime * rotationSpeed).eulerAngles;

        rotationPivot.rotation = Quaternion.Euler(0f, rotation.y, 0f);

        if (fireCountDown <= 0f)
        {
            Shoot();
            fireCountDown = 1f / fireRate;
        }

        fireCountDown -= Time.deltaTime;

	}

    void Shoot()
    {
        GameObject bulletGameObject = (GameObject)Instantiate(bulletPrefab, firePoint.position, firePoint.rotation);
        bulletMovement bullet = bulletGameObject.GetComponent<bulletMovement>();

        if (bullet != null)
            bullet.bulletTarget(target);
    }

    void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.blue;
        Gizmos.DrawWireSphere(transform.position, rangeTurret);
    }
}
